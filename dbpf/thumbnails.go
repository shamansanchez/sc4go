package dbpf

// GetThumbnails returns the region view thumbnail data for a city
// TGI: 0x8A2482B9. 0x4A2482BB, 0x00000000-0x00000006
func GetThumbnails(city DBPF) [][]byte {
	data := make([][]byte, 4)
	data[0] = city.FileContents["8A2482B9.4A2482BB.0"]
	data[1] = city.FileContents["8A2482B9.4A2482BB.2"]
	data[2] = city.FileContents["8A2482B9.4A2482BB.4"]
	data[3] = city.FileContents["8A2482B9.4A2482BB.6"]

	return data
}
